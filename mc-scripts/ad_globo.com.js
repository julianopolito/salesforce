// ==UserScript==
// @name         Ad Globo.com
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Juliano Polito
// @match        https://www.globo.com/*
// @grant        none
// @require		 https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// ==/UserScript==

/******************************************
*************** INSTRUCTIONS **************

To use the script just set USER PARAMETERS (line 40+):

- linkURL - destination URL for the banner. Leave empty '' if you don't want a link.
- imgURL - a public internet URL for the banner. I usually publish to Content Builder and get the direct URL to the image.
- newHeadline - A text to be used as the main headline for globo.com homepage (to avoid controversial news the could compromise the demo). Leave empty '' if you don't want to replace it.

The banner image can be any size, but for top banners it is better to use a horizontal format like Leaderboard (728x90) or Billboard (970x250).

Standard Banner sizes for reference:
https://blog.bannersnack.com/banner-standard-sizes/


********************************************/

(function() {
    'use strict';

    //Headline containers
    var hl_containerSelector = 'p.hui-premium__title:first';
    var hl_containerEL = $(hl_containerSelector);
    hl_containerEL.hide();

    $(function(){

        /*****************************
        ****** USER PARAMETERS *******
        ******************************/

        var linkURL = 'https://pub.s4.exacttarget.com/qnvkpr2ifez';
        var imgURL = 'http://image.s4.exct.net/lib/fe851574726c0d7b72/m/1/8144b51d-61ef-4d68-b9fe-47866a1b96cb.jpg';
        var newHeadline = 'Meio ambiente suspende ONGs em contratos';


        //SETUP VARS
        //CSS ID for the banner container
	    var containerSelector = '#banner_slb_topo';
	    var containerEL = $(containerSelector);
	    containerEL.hide();
	    //containerEL.attr('id','asdfghj-23456789');
        //Headline replace
        hl_containerEL.show();
        if(newHeadline != ''){
            hl_containerEL.text(newHeadline);
        }
	    //var adW = 1000;
	    //var adH = 260;
        //Style for responsive image
        var sty = $('<style>');
        sty.html('.responsive {width: 100%;height: auto;}');

        //External div container for image
	    var div = $('<div>');
	    div.css('width','100%');
	    //div.css('height', adH)
	    div.css('text-align', 'center');
        div.css('margin-bottom', '20px');

        //Img element for the banner
	    //var imgURL = 'http://image.s4.exct.net/lib/fe851574726c0d7b72/m/1/8144b51d-61ef-4d68-b9fe-47866a1b96cb.jpg';
	    var imgEL = $('<img>');
	    imgEL.attr('src', imgURL);
	    //imgEL.css('width',adW);
	    //imgEL.css('height', adH);
        imgEL.addClass('responsive');
        //Lin element for redirect
	    var linkEL = $('<a>').css('text-decoration', 'none').css('cursor', 'pointer').attr('href', linkURL).attr('target', '_blank');


	    if(linkURL != ''){
	    	linkEL.append(imgEL);
	    	div.append(linkEL);
	    }else{
	    	div.append(imgEL);
	    }
        $('head').append(sty);
	    //containerEL.empty();
	    containerEL.after(div);
	    containerEL.remove();
	});

})();
